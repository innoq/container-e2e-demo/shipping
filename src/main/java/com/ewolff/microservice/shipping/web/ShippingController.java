package com.ewolff.microservice.shipping.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

import com.ewolff.microservice.shipping.ShipmentRepository;

@Controller
public class ShippingController {

	private ShipmentRepository shipmentRepository;

	@Autowired
	public ShippingController(ShipmentRepository shipmentRepository, final MeterRegistry registry) {
		this.shipmentRepository = shipmentRepository;

		Gauge
            .builder("service_shipments_total", shipmentRepository, ShipmentRepository::count)
			.description("Total shipments")
			.tag("metric_type", "business")
            .register(registry);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView shipment(@PathVariable("id") long id) {
		return new ModelAndView("shipment", "shipment", shipmentRepository.findById(id).get());
	}

	@RequestMapping("/")
	public ModelAndView shipmentList() {
		return new ModelAndView("shipmentlist", "shipments", shipmentRepository.findAll());
	}

}
