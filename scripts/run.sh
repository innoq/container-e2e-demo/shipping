#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
PARENT_FOLDER=$(realpath $(dirname ${SCRIPT_FOLDER})/..)
PROJECT_NAME=$(basename ${PARENT_FOLDER})

echo "SCRIPT_FOLDER: ${SCRIPT_FOLDER}"
echo "PARENT_FOLDER: ${PARENT_FOLDER}"
echo "PROJECT_NAME: ${PROJECT_NAME}"

docker rm -f ${PROJECT_NAME}
docker run -d -p 8082:8080 \
    -v ${PARENT_FOLDER}/target:/run/ \
    -w /run \
    --name ${PROJECT_NAME} \
    -e DATABASE_HOST=172.17.0.1 \
    -e ORDER_HOST=172.17.0.1:8080 \
    -e DATABASE_NAME=db${PROJECT_NAME} \
    -e DATABASE_USERNAME=dbuser \
    -e DATABASE_PASSWORD=dbpass \
    openjdk:11-jre-slim \
    java -Xmx300m -Xms300m -XX:TieredStopAtLevel=1 -noverify -jar /run/microservice-istio-${PROJECT_NAME}-1.0.0.jar