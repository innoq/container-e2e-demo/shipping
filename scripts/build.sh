#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
PARENT_FOLDER=$(realpath $(dirname ${SCRIPT_FOLDER})/..)
PROJECT_NAME=$(basename ${PARENT_FOLDER})

echo "SCRIPT_FOLDER: ${SCRIPT_FOLDER}"
echo "PARENT_FOLDER: ${PARENT_FOLDER}"
echo "PROJECT_NAME: ${PROJECT_NAME}"

docker volume create --name maven-repo
docker run -it --rm \
    -v maven-repo:/root/.m2 \
    -v ${PARENT_FOLDER}:/run \
    -w /run \
    maven:3.6-jdk-11-slim \
    mvn clean install